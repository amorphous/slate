const SmeeClient = require('smee-client')

if (!process.env.WEBHOOK_TEST) {
	console.error("Please set process.env.WEBHOOK_TEST to the URL used for testing.")
	process.exit(1)
}

const smee = new SmeeClient({
  source: 'https://smee.io/SCnMYEERIpaNd6u',
  target: 'http://localhost:3000/gitlab',
  logger: console
})

const events = smee.start()