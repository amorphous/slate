require('dotenv').config()

const Mastodon = require('megalodon').default
 
const BASE_URL = 'https://botsin.space'
 
const access_token = process.env.ACCESS_TOKEN
 
const client = new Mastodon(access_token, BASE_URL + '/api/v1')

client.get('/timelines/home').then(res => console.log(res.data))