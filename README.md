# Slate

A post-git push feed tool.

Takes a webhook from Gitlab (TODO: Github integration) and then wraps it up as a Mastodon post (TODO: add to other feeds), and creates a running log of updates.

## Reading

https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#testing-webhooks
