require('dotenv').config()

const express = require('express')
const bodyParser = require("body-parser")
const Mastodon = require('megalodon').default
 
const BASE_URL = process.env.INSTANCE || 'https://botsin.space'
const access_token = process.env.ACCESS_TOKEN
const client = new Mastodon(access_token, BASE_URL + '/api/v1')

const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.json())

app.post('/gitlab', (req, res) => {
  const commits = req.body.commits

  // go in chronological order
  commits.reverse()
  for (let commit of commits) {
    if (process.env.AUTHOR && commit.author && commit.author.email !== process.env.AUTHOR) {
      continue;
    }

    // create a post per commit
    const namespace = req.body.project.namespace
    const project = req.body.project.name
    const message = commit.message

    const post = `${project}: ${message.trim()}\n\n#${namespace.toLowerCase()}`
    client.post('/statuses', {
      status: post,
      visibility: 'unlisted'
    })
  }

  res.sendStatus(200)
})

app.listen(port, () => console.log(`Post Commit Feed Tool listening on port ${port}`))